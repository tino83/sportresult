package com.sport.results;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;

import com.sport.results.cli.Arguments;
import com.sport.results.cli.ArgumentsParser;
import com.sport.results.common.ArgumentInterpreter;
import com.sport.results.common.config.Configuration;
import com.sport.results.common.io.MatchSummaryFilenameFilter;
import com.sport.results.container.Container;
import com.sport.results.model.io.reader.MatchSummaryReader;
import com.sport.results.model.report.GroupStanding;
import com.sport.results.model.report.MatchReportFormatter;
import com.sport.results.model.report.MatchResult;
import com.sport.results.model.report.MatchResultsReport;
import com.sport.results.model.report.Scorer;
import com.sport.results.model.summary.MatchSummary;

public class Main {

	public static void main(String[] args) {
		System.out.println("Running utility:");
		System.out.print("Parsing arguments... ");
		Arguments arguments = new ArgumentsParser().parseArguments(args);
		System.out.println("Done.");
		run(new ArgumentInterpreter(arguments));
		System.out.println("Utility finished.");
	}

	public static void run(ArgumentInterpreter args) {
		if (args.getHelp()) {
			printUsage(System.out);
		} else {
			String configFile = args.getConfigFile();
			if (configFile != null) {
				Container.createInstance(new Configuration(configFile));
			} else {
				System.out.println("Config file not specified!");
			}
			String inputFolder = args.getInputFolder();
			if (inputFolder != null) {
				processFilesFromDirectory(inputFolder);
			} else {
				System.out.println("Input folder not specified!");
			}
			showReports();
		}
	}

	public static void processFilesFromDirectory(String dir) {
		File d = new File(dir);
		if (d.isDirectory()) {
			MatchSummaryReader msr = Container.getInstance().getMatchSummaryReader();
			for (File f : d.listFiles(new MatchSummaryFilenameFilter())){
				try {
					System.out.print("Reading file " + f.getName() + "... ");
					MatchSummary ms = msr.read(new FileInputStream(f));
					if (ms != null) {
						Container.getInstance().getDBWriter().writeMatchSummary(ms);
						System.out.println("Done.");
					}					
				} catch(Exception e) {
					e.printStackTrace(System.out);
				}
			}
		} else {
			System.out.println("Invalid directory \"" + dir + "\"!");
		}
	}

	public static void showReports() {
		MatchResultsReport report = Container.getInstance().getDBReader().readMatchReport();
		MatchReportFormatter formatter = Container.getInstance().getMatchReportFormatter();
		System.out.println("-----------------------");
		System.out.println("Showing results:");
		for (MatchResult mr : report.getResults()) {
			System.out.println(formatter.formatMatchResult(mr));
		}
		System.out.println("-----------------------");
		System.out.println("Showing top scorers:");
		for (String s : formatter.getTopScorersHeader()) {
			System.out.println(s);
		}
		for (Scorer s : report.getTopScorers()) {
			System.out.println(formatter.formatScorer(s));
		}
		System.out.println("-----------------------");
		System.out.println("Showing group standings:");
		for (String s : formatter.getGroupStandingsHeader()) {
			System.out.println(s);
		}
		for (GroupStanding gs : report.getGroupStandings()) {
			System.out.println(formatter.formatGroupStanding(gs));
		}
	}

	private static void printUsage(PrintStream out) {
		out.println("Usage:");
		out.println("  Main -i inputFolder -c configFile [-h]");
		out.println();
		out.println("Options:");
		out.println("  -i inputFolder        Folder with input match summary files.");
		out.println("  -c configFile         Fullpath file name to config file.");
		out.println("  -h                    Print usage and exit program.");
		out.println();;
	}

}
