package com.sport.results.model.report;

public interface Team {

	public String getCode();

	public String getName();

}
