package com.sport.results.model.report;

public interface Player {

	public Team getTeam();

	public String getName();

}
