package com.sport.results.model.report;

public interface GroupStanding {

	public Team getTeam();

	public int getWins();

	public int getTies();

	public int getLoses();

	public int getPoints();

	public int getGoalsFor();

	public int getGoalsAgainst();

}
