package com.sport.results.model.report;

public interface MatchResultsReport {

	public Iterable<MatchResult> getResults();

	public Iterable<Scorer> getTopScorers();

	public Iterable<GroupStanding> getGroupStandings();

}
