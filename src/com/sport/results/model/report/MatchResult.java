package com.sport.results.model.report;

public interface MatchResult {

	public Team getHomeTeam();

	public Team getAwayTeam();

	public int getHomeGoalCount();

	public int getAwayGoalCount();

}
