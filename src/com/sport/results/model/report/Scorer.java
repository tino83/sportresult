package com.sport.results.model.report;

public interface Scorer {

	public Player getPlayer();

	public int getGoalCount();

}
