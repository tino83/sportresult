package com.sport.results.model.report;

public interface MatchReportFormatter {

	public String formatMatchResult(MatchResult matchResult);

	public String[] getTopScorersHeader();

	public String formatScorer(Scorer scorer);

	public String[] getGroupStandingsHeader();

	public String formatGroupStanding(GroupStanding standing);

}
