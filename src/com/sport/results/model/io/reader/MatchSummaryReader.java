package com.sport.results.model.io.reader;

import java.io.InputStream;

import com.sport.results.model.summary.MatchSummary;

public interface MatchSummaryReader {

	public MatchSummary read(InputStream is);

}
