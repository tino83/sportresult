package com.sport.results.model.io.parser;

import com.sport.results.model.summary.Lineup;
import com.sport.results.model.summary.TeamStats;

public interface StatsParser {

	public TeamStats parseStats(String s, Lineup lineup);

}
