package com.sport.results.model.io.parser;

import com.sport.results.model.summary.Details;

public interface DetailsParser {

	public Details parseDetails(String s);

}
