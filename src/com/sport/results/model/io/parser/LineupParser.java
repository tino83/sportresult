package com.sport.results.model.io.parser;

import com.sport.results.model.summary.Lineup;

public interface LineupParser {

	public Lineup parseLineup(String s);

}
