package com.sport.results.model.io.parser;

public interface Parsers {

	public DetailsParser getDetailsParser();

	public EventsParser getEventsParser();

	public LineupParser getLineupParser();

	public TeamParser getTeamParser();

	public StatsParser getStatsParser();

}
