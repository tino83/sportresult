package com.sport.results.model.io.parser;

import com.sport.results.model.summary.Events;
import com.sport.results.model.summary.Lineup;

public interface EventsParser {

	public Events parseEvents(String s, Lineup lineup);

}
