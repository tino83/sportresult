package com.sport.results.model.io.parser;

public abstract class AbstractParsers implements Parsers {

	private final DetailsParser detailsParser;
	private final EventsParser eventsParser;
	private final LineupParser lineupParser;
	private final TeamParser teamParser;
	private final StatsParser statsParser;
	
	public AbstractParsers(DetailsParser detailsParser, EventsParser eventsParser, LineupParser lineupParser,
			TeamParser teamParser, StatsParser statsParser) {
		this.detailsParser = detailsParser;
		this.eventsParser = eventsParser;
		this.lineupParser = lineupParser;
		this.teamParser = teamParser;
		this.statsParser = statsParser;
	}

	@Override
	public DetailsParser getDetailsParser() {
		return this.detailsParser;
	}

	@Override
	public EventsParser getEventsParser() {
		return this.eventsParser;
	}

	@Override
	public LineupParser getLineupParser() {
		return this.lineupParser;
	}

	@Override
	public TeamParser getTeamParser() {
		return this.teamParser;
	}

	@Override
	public StatsParser getStatsParser() {
		return this.statsParser;
	}

}
