package com.sport.results.model.io.parser;

import com.sport.results.model.summary.Team;

public interface TeamParser {

	public Team parseTeam(String s);

}
