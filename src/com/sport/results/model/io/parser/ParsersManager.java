package com.sport.results.model.io.parser;

import com.sport.results.common.io.MatchSummaryFileFormat;
import com.sport.results.container.factory.summary.MatchSummaryFactory;

public interface ParsersManager {

	public Parsers getParsersForFormat(MatchSummaryFileFormat format);

	public MatchSummaryFactory getMatchSummaryFactory();

}
