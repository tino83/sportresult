package com.sport.results.model.summary;

public interface Player {

	public int getNumber();

	public String getName();

}
