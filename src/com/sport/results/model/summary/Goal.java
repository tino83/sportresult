package com.sport.results.model.summary;

public interface Goal {

	public int getMinute();

	public Player getPlayer();

}
