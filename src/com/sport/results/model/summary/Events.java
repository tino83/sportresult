package com.sport.results.model.summary;

public interface Events {

	public Iterable<Card> getCards();

	public Iterable<Goal> getGoals();

	public Iterable<Substitution> getSubstitutions();

}
