package com.sport.results.model.summary;

public interface Substitution {

	public int getMinute();

	public Player getOffPlayer();

	public Player getOnPlayer();

}
