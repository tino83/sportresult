package com.sport.results.model.summary;

import com.sport.results.common.CardType;

public interface Card {

	public CardType getCardType();

	public int getMinute();

	public Player getPlayer();

}
