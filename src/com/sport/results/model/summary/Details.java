package com.sport.results.model.summary;

import java.time.LocalDateTime;

public interface Details {

	public LocalDateTime getDateTime();

	public String getPlace();

}
