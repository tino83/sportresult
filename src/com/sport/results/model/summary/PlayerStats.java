package com.sport.results.model.summary;

public interface PlayerStats {

	public int getNumberOfShotsOnTarget();

	public int getNumberOfShotsWide();

	public int getNumberOfFouls();

	public int getNumberOfPassesCompleted();

	public int getNumberOfPassesIntercepted();

}
