package com.sport.results.model.summary;

public interface MatchSummary {

	public Details getDetails();

	public Team getHomeTeam();

	public Team getAwayTeam();

	public Lineup getHomeLineup();

	public Lineup getAwayLineup();

	public Events getHomeEvents();

	public Events getAwayEvents();

	public TeamStats getHomeTeamStats();

	public TeamStats getAwayTeamStats();

}
