package com.sport.results.model.summary;

public interface Team {

	public String getCode();

	public String getName();

}
