package com.sport.results.model.summary;

public interface TeamStats {

	public PlayerStats getPlayerStats(Player player);

}
