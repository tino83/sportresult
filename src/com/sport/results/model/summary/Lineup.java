package com.sport.results.model.summary;

public interface Lineup {

	public Iterable<Player> getStartingPlayers();

	public Iterable<Player> getSubstitutePlayers();

	public Player getPlayerByNumber(int number);

	public Player getPlayerByOrdinal(int ordinal);

}
