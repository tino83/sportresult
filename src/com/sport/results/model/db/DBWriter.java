package com.sport.results.model.db;

import com.sport.results.model.summary.MatchSummary;

public interface DBWriter {

	public void writeMatchSummary(MatchSummary summary);

}
