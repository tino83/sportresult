package com.sport.results.model.db;

import com.sport.results.model.report.MatchResultsReport;

public interface DBReader {

	public MatchResultsReport readMatchReport();

}
