package com.sport.results.util;

public class StringUtils {

	public static String leftPad(String text, int count) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count - text.length(); i++) {
			sb.append(" ");
		}
		sb.append(text);
		return sb.toString();
	}

	public static String rightPad(String text, int count) {
		StringBuilder sb = new StringBuilder(text);
		for (int i = 0; i < count - text.length(); i++) {
			sb.append(" ");
		}
		return sb.toString();		
	}

}
