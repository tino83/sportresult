package com.sport.results.common;

public enum CardType {

	YELLOW("Y"), RED("R");
	
	private final String code;

	private CardType(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

}
