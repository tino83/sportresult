package com.sport.results.common.io;

public enum MatchSummaryFileFormat {

	SEPARATED("SEPARATED"), FIXEDWIDTH("FIXEDWIDTH");

	private final String name;

	private MatchSummaryFileFormat(String name) {
		this.name = name;
	}

	public static MatchSummaryFileFormat parseFormat(String name) {
		for (MatchSummaryFileFormat f : MatchSummaryFileFormat.values()) {
			if (name.equalsIgnoreCase(f.name)) {
				return f;
			}
		}
		throw new RuntimeException("Unsupported file format!");
	}

}
