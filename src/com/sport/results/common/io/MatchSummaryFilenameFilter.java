package com.sport.results.common.io;

import java.io.File;
import java.io.FilenameFilter;

public class MatchSummaryFilenameFilter implements FilenameFilter {
	
	@Override
	public boolean accept(File dir, String name) {
		return name.endsWith(".sum");
	}

}
