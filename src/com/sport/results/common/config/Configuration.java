package com.sport.results.common.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class Configuration {

	private final Map<String, String> properties = new HashMap<String, String>();

	public Configuration(String configurationFileName) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(configurationFileName)));
			String s = br.readLine();
			while (s != null) {
				String[] tokens = s.split("=");
				this.properties.put(tokens[0], tokens[1]);
				s = br.readLine();
			}
			br.close();
		} catch(Exception e) {
			System.out.println("Failed to parse configuration!");
		}
	}

	public String getProperty(String key) {
		return this.properties.get(key);
	}

}
