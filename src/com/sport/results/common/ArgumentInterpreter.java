package com.sport.results.common;

import com.sport.results.cli.Argument;
import com.sport.results.cli.ArgumentCode;
import com.sport.results.cli.Arguments;
import com.sport.results.cli.Parameter;

public class ArgumentInterpreter {

	private final Arguments arguments;

	public ArgumentInterpreter(Arguments arguments) {
		this.arguments = arguments;
	}

	public boolean getHelp() {
		return this.arguments.containsArgument(ArgumentCode.HELP);
	}

	public String getInputFolder() {
		return getParameterValue(ArgumentCode.INPUT_FOLDER);
	}

	public String getConfigFile() {
		return getParameterValue(ArgumentCode.CONFIG_FILE);
	}

	private String getParameterValue(ArgumentCode code) {
		Argument a = this.arguments.getArgument(code);
		if (a != null && a instanceof Parameter) {
			return ((Parameter)a).getValue();
		} else {
			return null;
		}
	}

}
