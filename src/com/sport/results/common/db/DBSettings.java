package com.sport.results.common.db;

import com.sport.results.common.config.Configuration;

public class DBSettings {

	private final String KEY_dbType = "db.type";
	private final String KEY_url = "db.url";
	private final String KEY_user = "db.user";
	private final String KEY_password = "db.password";

	private final DBType dbType;
	private final String url;
	private final String user;
	private final String password;

	public DBSettings(Configuration configuration) {
		this.dbType = DBType.parseType(configuration.getProperty(KEY_dbType));
		this.url = configuration.getProperty(KEY_url);
		this.user = configuration.getProperty(KEY_user);
		this.password = configuration.getProperty(KEY_password);
	}

	public DBType getDBType() {
		return this.dbType;
	}

	public String getUrl() {
		return this.url;
	}

	public String getUser() {
		return this.user;
	}

	public String getPassword() {
		return this.password;
	}

}
