package com.sport.results.common.db;

public enum DBType {

	ORACLE("oracle"), MSSQL("mssql"), MYSQL("mysql");

	private final String name;

	private DBType(String name) {
		this.name = name;
	}

	public static DBType parseType(String name) {
		for (DBType d : DBType.values()) {
			if (name.equalsIgnoreCase(d.name)) {
				return d;
			}
		}
		throw new RuntimeException("Unsupported DB type!");
	}

}
