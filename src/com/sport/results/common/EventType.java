package com.sport.results.common;

public enum EventType {

	CARD, GOAL, SUBSTITUTION, UNKNOWN;

	public static EventType getEventType(String s) {
		if (s.equalsIgnoreCase("Y")) {
			return EventType.CARD;
		} else if (s.equalsIgnoreCase("G")) {
			return EventType.GOAL;
		} else if (s.equalsIgnoreCase("S")) {
			return EventType.SUBSTITUTION;
		} else {
			return EventType.UNKNOWN;
		}
	}

}
