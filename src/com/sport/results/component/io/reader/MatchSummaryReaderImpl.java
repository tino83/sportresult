package com.sport.results.component.io.reader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.sport.results.common.io.MatchSummaryFileFormat;
import com.sport.results.model.io.parser.Parsers;
import com.sport.results.model.io.parser.ParsersManager;
import com.sport.results.model.io.reader.MatchSummaryReader;
import com.sport.results.model.summary.Details;
import com.sport.results.model.summary.Events;
import com.sport.results.model.summary.Lineup;
import com.sport.results.model.summary.MatchSummary;
import com.sport.results.model.summary.Team;
import com.sport.results.model.summary.TeamStats;

public class MatchSummaryReaderImpl implements MatchSummaryReader {

	private final ParsersManager parsersManager;

	public MatchSummaryReaderImpl(ParsersManager parsersManager) {
		this.parsersManager = parsersManager;
	}

	@Override
	public MatchSummary read(InputStream is) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String s = br.readLine();
			String[] tokens = s.split("=");
			Parsers parsers = this.parsersManager.getParsersForFormat(MatchSummaryFileFormat.parseFormat(tokens[1]));
			if (parsers == null) {
				br.close();
				System.out.println("Unsupported format!");
				return null;
			} else {
				Map<String, String> lines = new HashMap<String, String>();
				s = br.readLine();
				while (s != null) {
					tokens = s.split("=");
					lines.put(tokens[0], tokens[1]);
					s = br.readLine();
				}
				br.close();
				Details details = parsers.getDetailsParser().parseDetails(lines.get("DETAILS"));
				Team homeTeam = parsers.getTeamParser().parseTeam(lines.get("TEAMHOME"));
				Team awayTeam = parsers.getTeamParser().parseTeam(lines.get("TEAMAWAY"));
				Lineup homeLineup = parsers.getLineupParser().parseLineup(lines.get("LINEUPHOME"));
				Lineup awayLineup = parsers.getLineupParser().parseLineup(lines.get("LINEUPAWAY"));
				Events homeEvents = parsers.getEventsParser().parseEvents(lines.get("EVENTSHOME"), homeLineup);
				Events awayEvents = parsers.getEventsParser().parseEvents(lines.get("EVENTSAWAY"), awayLineup);
				TeamStats homeTeamStats = parsers.getStatsParser().parseStats(lines.get("STATSHOME"), homeLineup);
				TeamStats awayTeamStats = parsers.getStatsParser().parseStats(lines.get("STATSAWAY"), awayLineup);
				return parsersManager.getMatchSummaryFactory().createMatchSummary(details, homeTeam, awayTeam,
						homeLineup, awayLineup, homeEvents, awayEvents, homeTeamStats, awayTeamStats);
			}
		} catch(Exception e) {
			//e.printStackTrace(System.out);
			System.out.println("Invalid format!");
			return null;
		}
	}

}
