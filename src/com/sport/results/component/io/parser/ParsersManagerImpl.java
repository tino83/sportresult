package com.sport.results.component.io.parser;

import java.util.HashMap;
import java.util.Map;

import com.sport.results.common.io.MatchSummaryFileFormat;
import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.Parsers;
import com.sport.results.model.io.parser.ParsersManager;

public class ParsersManagerImpl implements ParsersManager {

	private final Map<MatchSummaryFileFormat, Parsers> parsers = new HashMap<MatchSummaryFileFormat, Parsers>();
	private final MatchSummaryFactory factory;

	public ParsersManagerImpl(MatchSummaryFactory factory) {
		this.parsers.put(MatchSummaryFileFormat.SEPARATED, new ParsersSeparated(factory));
		this.factory = factory;
	}

	@Override
	public Parsers getParsersForFormat(MatchSummaryFileFormat format) {
		return this.parsers.get(format);
	}

	@Override
	public MatchSummaryFactory getMatchSummaryFactory() {
		return this.factory;
	}

}
