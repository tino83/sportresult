package com.sport.results.component.io.parser;

import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.DetailsParser;
import com.sport.results.model.summary.Details;

public class DetailsParserSeparated implements DetailsParser {

	private final MatchSummaryFactory factory;

	public DetailsParserSeparated(MatchSummaryFactory factory) {
		this.factory = factory;
	}

	@Override
	public Details parseDetails(String s) {
		String[] tokens = s.split("\\|");
		return this.factory.createDetails(tokens[0], tokens[1]);
	}

}
