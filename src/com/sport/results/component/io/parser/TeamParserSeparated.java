package com.sport.results.component.io.parser;

import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.TeamParser;
import com.sport.results.model.summary.Team;

public class TeamParserSeparated implements TeamParser {

	private final MatchSummaryFactory factory;

	public TeamParserSeparated(MatchSummaryFactory factory) {
		this.factory = factory;
	}

	@Override
	public Team parseTeam(String s) {
		String[] tokens = s.split("\\|");
		return this.factory.createTeam(tokens[0], tokens[1]);
	}

}
