package com.sport.results.component.io.parser;

import java.util.ArrayList;
import java.util.List;

import com.sport.results.common.EventType;
import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.EventsParser;
import com.sport.results.model.summary.Events;
import com.sport.results.model.summary.Lineup;

public class EventsParserSeparated implements EventsParser {

	private final MatchSummaryFactory factory;

	public EventsParserSeparated(MatchSummaryFactory factory) {
		this.factory = factory;
	}

	@Override
	public Events parseEvents(String s, Lineup lineup) {
		String[] tokens = s.split("\\|");
		List<Integer> cardIndexes = new ArrayList<Integer>();
		List<Integer> goalIndexes = new ArrayList<Integer>();
		List<Integer> substitutionIndexes = new ArrayList<Integer>();
		for (int i = 0; i < tokens.length; i++) {
			switch (EventType.getEventType(tokens[i].substring(0,1))) {
				case CARD: cardIndexes.add(i); break;
				case GOAL: goalIndexes.add(i); break;
				case SUBSTITUTION: substitutionIndexes.add(i); break;
				default: continue;
			}
		}
		String[][] cards = new String[cardIndexes.size()][3];
		String[][] goals = new String[goalIndexes.size()][2];
		String[][] substitutions = new String[1][3];
		for (int i = 0; i < cardIndexes.size(); i++) {
			String[] t = tokens[cardIndexes.get(i)].split(",");
			for (int j = 0; j < t.length; j++) {
				cards[i][j] = t[j];
			}
		}
		for (int i = 0; i < goalIndexes.size(); i++) {
			String[] t = tokens[goalIndexes.get(i)].split(",");
			for (int j = 1; j < t.length; j++) {
				goals[i][j - 1] = t[j];
			}
		}
		for (int i = 0; i < substitutionIndexes.size(); i++) {
			String[] t = tokens[substitutionIndexes.get(i)].split(",");
			for (int j = 1; j < t.length; j++) {
				substitutions[i][j - 1] = t[j];
			}
		}
		if (substitutionIndexes.size() == 0) {
			substitutions = new String[0][3];
		}
		return this.factory.createEvents(cards, goals, substitutions, lineup);
	}

}
