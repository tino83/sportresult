package com.sport.results.component.io.parser;

import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.StatsParser;
import com.sport.results.model.summary.Lineup;
import com.sport.results.model.summary.TeamStats;

public class StatsParserSeparated implements StatsParser {

	private final MatchSummaryFactory factory;

	public StatsParserSeparated(MatchSummaryFactory factory) {
		this.factory = factory;
	}

	@Override
	public TeamStats parseStats(String s, Lineup lineup) {
		String[] tokens = s.split("~");
		String[] shotsTokens = tokens[0].split("\\|");
		String[] foulsTokens = tokens[1].split("\\|");
		String[] passesTokens = tokens[2].split("\\|");
		String[] shotsPairs = shotsTokens[1].split(",");
		String[][] shots = new String[shotsPairs.length][2];
		for (int i = 0; i < shotsPairs.length; i++) {
			shots[i] = shotsPairs[i].split("/");
		}
		String[] fouls = foulsTokens[1].split(",");
		String[] passesPairs = passesTokens[1].split(",");
		String[][] passes = new String[passesPairs.length][2];
		for (int i = 0; i < passesPairs.length; i++) {
			passes[i] = passesPairs[i].split("/");
		}
		return this.factory.createTeamStats(shots, fouls, passes, lineup);
	}

}
