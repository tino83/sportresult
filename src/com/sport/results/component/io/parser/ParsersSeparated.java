package com.sport.results.component.io.parser;

import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.AbstractParsers;

public class ParsersSeparated extends AbstractParsers {

	public ParsersSeparated(MatchSummaryFactory factory) {
		super(new DetailsParserSeparated(factory), new EventsParserSeparated(factory),
				new LineupParserSeparated(factory), new TeamParserSeparated(factory),
				new StatsParserSeparated(factory));
	}

}
