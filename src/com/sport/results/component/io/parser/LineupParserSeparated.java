package com.sport.results.component.io.parser;

import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.io.parser.LineupParser;
import com.sport.results.model.summary.Lineup;

public class LineupParserSeparated implements LineupParser {

	private final MatchSummaryFactory factory;

	public LineupParserSeparated(MatchSummaryFactory factory) {
		this.factory = factory;
	}

	@Override
	public Lineup parseLineup(String s) {
		String[] tokens = s.split("~");
		String[] startingTokens = tokens[0].split("\\|");
		String[] substituteTokens = tokens[1].split("\\|");
		String[][] starting = new String[6][2];
		String[][] substitute = new String[3][2];
		for (int i = 0; i < 6; i++) {
			String[] t = startingTokens[i].split(",");
			starting[i][0] = t[0];
			starting[i][1] = t[1];
		}
		for (int i = 0; i < 3; i++) {
			String[] t = substituteTokens[i].split(",");
			substitute[i][0] = t[0];
			substitute[i][1] = t[1];
		}
		return this.factory.createLineup(starting, substitute);
	}

}
