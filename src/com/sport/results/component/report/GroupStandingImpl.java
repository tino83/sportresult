package com.sport.results.component.report;

import com.sport.results.model.report.GroupStanding;
import com.sport.results.model.report.Team;

public class GroupStandingImpl implements GroupStanding {

	private final Team team;
	private final int wins;
	private final int ties;
	private final int loses;
	private final int points;
	private final int goalsFor;
	private final int goalsAgainst;

	public GroupStandingImpl(Team team, int wins, int ties, int loses, int points, int goalsFor, int goalsAgainst) {
		this.team = team;
		this.wins = wins;
		this.ties = ties;
		this.loses = loses;
		this.points = points;
		this.goalsFor = goalsFor;
		this.goalsAgainst = goalsAgainst;
	}
	
	@Override
	public Team getTeam() {
		return this.team;
	}

	@Override
	public int getWins() {
		return this.wins;
	}

	@Override
	public int getTies() {
		return this.ties;
	}

	@Override
	public int getLoses() {
		return this.loses;
	}

	@Override
	public int getPoints() {
		return this.points;
	}

	@Override
	public int getGoalsFor() {
		return this.goalsFor;
	}

	@Override
	public int getGoalsAgainst() {
		return this.goalsAgainst;
	}

}
