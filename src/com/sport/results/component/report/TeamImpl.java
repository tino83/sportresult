package com.sport.results.component.report;

import com.sport.results.model.report.Team;

public class TeamImpl implements Team {

	private final String code;
	private final String name;

	public TeamImpl(String code, String name) {
		this.code = code;
		this.name = name;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
