package com.sport.results.component.report;

import java.util.List;

import com.sport.results.model.report.GroupStanding;
import com.sport.results.model.report.MatchResult;
import com.sport.results.model.report.MatchResultsReport;
import com.sport.results.model.report.Scorer;

public class MatchResultsReportImpl implements MatchResultsReport {

	private final List<MatchResult> results;
	private final List<Scorer> scorers;
	private final List<GroupStanding> groupStandings;

	public MatchResultsReportImpl(List<MatchResult> results, List<Scorer> scorers, List<GroupStanding> groupStandings) {
		this.results = results;
		this.scorers = scorers;
		this.groupStandings = groupStandings;
	}

	@Override
	public Iterable<MatchResult> getResults() {
		return this.results;
	}

	@Override
	public Iterable<Scorer> getTopScorers() {
		return this.scorers;
	}

	@Override
	public Iterable<GroupStanding> getGroupStandings() {
		return this.groupStandings;
	}

}
