package com.sport.results.component.report;

import com.sport.results.model.report.Player;
import com.sport.results.model.report.Team;

public class PlayerImpl implements Player {

	private final Team team;
	private final String name;

	public PlayerImpl(Team team, String name) {
		this.team = team;
		this.name = name;
	}

	@Override
	public Team getTeam() {
		return this.team;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
