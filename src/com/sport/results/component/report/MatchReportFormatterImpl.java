package com.sport.results.component.report;

import com.sport.results.model.report.GroupStanding;
import com.sport.results.model.report.MatchReportFormatter;
import com.sport.results.model.report.MatchResult;
import com.sport.results.model.report.Scorer;
import com.sport.results.util.StringUtils;

public class MatchReportFormatterImpl implements MatchReportFormatter {

	@Override
	public String formatMatchResult(MatchResult matchResult) {
		return matchResult.getHomeTeam().getName() + " " + matchResult.getHomeGoalCount() +
				":" + matchResult.getAwayGoalCount() + " " + matchResult.getAwayTeam().getName();
	}

	@Override
	public String[] getTopScorersHeader() {
		return new String[]{StringUtils.rightPad("NAME", 15) + " | GOALS",
				"-----------------------"
		};
	}

	@Override
	public String formatScorer(Scorer scorer) {
		return StringUtils.rightPad(scorer.getPlayer().getName(),  15) + " |" + StringUtils.leftPad("" + scorer.getGoalCount(), 6);
	}

	@Override
	public String[] getGroupStandingsHeader() {
		return new String[]{StringUtils.rightPad("TEAM", 15) + " |  W |  T |  L |  P | GF | GA",
				"---------------------------------------------"
		};
	}

	@Override
	public String formatGroupStanding(GroupStanding standing) {
		return StringUtils.rightPad(standing.getTeam().getName(),  15) + " |" + StringUtils.leftPad("" + standing.getWins(), 3) + 
				" |" + StringUtils.leftPad("" + standing.getTies(), 3) + " |" + StringUtils.leftPad("" + standing.getLoses(), 3) +
				" |" + StringUtils.leftPad("" + standing.getPoints(), 3) + " |" + StringUtils.leftPad("" + standing.getGoalsFor(), 3) +
				" |" + StringUtils.leftPad("" + standing.getGoalsAgainst(), 3);
	}

}
