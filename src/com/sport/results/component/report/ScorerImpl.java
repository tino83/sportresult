package com.sport.results.component.report;

import com.sport.results.model.report.Player;
import com.sport.results.model.report.Scorer;

public class ScorerImpl implements Scorer {

	private final Player player;
	private final int goalCount;

	public ScorerImpl(Player player, int goalCount) {
		this.player = player;
		this.goalCount = goalCount;
	}

	@Override
	public Player getPlayer() {
		return this.player;
	}

	@Override
	public int getGoalCount() {
		return this.goalCount;
	}

}
