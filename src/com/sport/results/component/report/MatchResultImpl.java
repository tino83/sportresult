package com.sport.results.component.report;

import com.sport.results.model.report.MatchResult;
import com.sport.results.model.report.Team;

public class MatchResultImpl implements MatchResult {

	private final Team homeTeam;
	private final Team awayTeam;
	private final int homeGoalsCount;
	private final int awayGoalsCount;

	public MatchResultImpl(Team homeTeam, Team awayTeam, int homeGoalsCount, int awayGoalsCount) {
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.homeGoalsCount = homeGoalsCount;
		this.awayGoalsCount = awayGoalsCount;
	}

	@Override
	public Team getHomeTeam() {
		return this.homeTeam;
	}

	@Override
	public Team getAwayTeam() {
		return this.awayTeam;
	}

	@Override
	public int getHomeGoalCount() {
		return this.homeGoalsCount;
	}

	@Override
	public int getAwayGoalCount() {
		return this.awayGoalsCount;
	}

}
