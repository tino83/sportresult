package com.sport.results.component.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.sport.results.container.db.ConnectionProvider;
import com.sport.results.container.factory.report.MatchResultFactory;
import com.sport.results.model.db.DBReader;
import com.sport.results.model.report.GroupStanding;
import com.sport.results.model.report.MatchResult;
import com.sport.results.model.report.MatchResultsReport;
import com.sport.results.model.report.Scorer;

public class DBReaderImpl implements DBReader {

	private final ConnectionProvider connectionProvider;
	private final MatchResultFactory matchResultFactory;

	private static String SELECT_MATCH_RESULTS_STMT = "SELECT " +
		" T1.TEAM_CD, T1.TEAM_NAME, (SELECT COUNT(*) FROM MATCH_GOALS G WHERE G.MATCH_ID = M.MATCH_ID AND G.TEAM_ID = M.HOME_TEAM_ID) AS HOME_GOAL_COUNT," +
		" T2.TEAM_CD, T2.TEAM_NAME, (SELECT COUNT(*) FROM MATCH_GOALS G WHERE G.MATCH_ID = M.MATCH_ID AND G.TEAM_ID = M.AWAY_TEAM_ID) AS AWAY_GOAL_COUNT " +
		" FROM MATCH_DETAILS M, MATCH_TEAMS T1, MATCH_TEAMS T2 WHERE T1.TEAM_ID = M.AWAY_TEAM_ID AND T2.TEAM_ID = M.HOME_TEAM_ID";

	private static String SELECT_TOP_SCORERS_STMT = "SELECT T.TEAM_CD, T.TEAM_NAME, P.PLAYER_NAME, SUM(G.GOAL_ID) AS GOAL_COUNT " +
		" FROM MATCH_GOALS G, MATCH_PLAYERS P, MATCH_TEAMS T " +
		" WHERE G.PLAYER_ID = P.PLAYER_ID AND T.TEAM_ID = P.TEAM_ID " +
		" GROUP BY T.TEAM_CD, T.TEAM_NAME, P.PLAYER_NAME " +
		" ORDER BY 4 DESC";

	private static String SELECT_GROUP_STANDINGS_STMT = "WITH MATCH_RESULTS AS (" +
			" SELECT X.HOME_TEAM_ID, X.AWAY_TEAM_ID," +
			" SUM(X.HOME_GOAL) AS HOME_GOALS," +
			" SUM(X.AWAY_GOAL) AS AWAY_GOALS" +
			" FROM" +
			" (SELECT M.HOME_TEAM_ID, M.AWAY_TEAM_ID," +
			" CASE WHEN (M.HOME_TEAM_ID=G.TEAM_ID) THEN 1 ELSE 0 END AS HOME_GOAL," +
			" CASE WHEN (M.AWAY_TEAM_ID=G.TEAM_ID) THEN 1 ELSE 0 END AS AWAY_GOAL" +
			" FROM MATCH_DETAILS M, MATCH_GOALS G" +
			" WHERE G.MATCH_ID = M.MATCH_ID) X" +
			" GROUP BY X.HOME_TEAM_ID, X.AWAY_TEAM_ID" +
			" )" +
			" SELECT" +
			" Z.TEAM_CD, Z.TEAM_NAME, SUM(Z.WIN) AS WINS, SUM(Z.TIE) AS TIES, SUM(Z.LOSE) AS LOSES," +
			" SUM(Z.WIN) * 3 + SUM(Z.TIE) AS POINTS," +
			" SUM(Z.GOALS_FOR) AS GOALS_FOR, SUM(Z.GOALS_AGAINST) AS GOALS_AGAINST" +
			" FROM (" +
			" SELECT" +
			" T.TEAM_CD, T.TEAM_NAME," +
			" CASE WHEN(R.HOME_GOALS > R.AWAY_GOALS) THEN 1 ELSE 0 END AS WIN," +
			" CASE WHEN(R.HOME_GOALS = R.AWAY_GOALS) THEN 1 ELSE 0 END AS TIE," +
			" CASE WHEN(R.HOME_GOALS < R.AWAY_GOALS) THEN 1 ELSE 0 END AS LOSE," +
			" HOME_GOALS AS GOALS_FOR," +
			" AWAY_GOALS AS GOALS_AGAINST" +
			" FROM MATCH_TEAMS T, MATCH_RESULTS R" +
			" WHERE R.HOME_TEAM_ID = T.TEAM_ID" +
			" UNION ALL" +
			" SELECT" +
			" T.TEAM_CD, T.TEAM_NAME," +
			" CASE WHEN (R.AWAY_GOALS > R.HOME_GOALS) THEN 1 ELSE 0 END AS WIN," +
			" CASE WHEN(R.AWAY_GOALS = R.HOME_GOALS) THEN 1 ELSE 0 END AS TIE," +
			" CASE WHEN(R.AWAY_GOALS < R.HOME_GOALS) THEN 1 ELSE 0 END AS LOSE," +
			" AWAY_GOALS AS GOALS_FOR," +
			" HOME_GOALS AS GOALS_AGAINST" +
			" FROM MATCH_TEAMS T, MATCH_RESULTS R" +
			" WHERE R.AWAY_TEAM_ID = T.TEAM_ID" +
			" ) Z" +
			" GROUP BY Z.TEAM_CD, Z.TEAM_NAME ORDER BY 6 DESC";

	public DBReaderImpl(ConnectionProvider connectionProvider, MatchResultFactory matchResultFactory) {
		this.connectionProvider = connectionProvider;
		this.matchResultFactory = matchResultFactory;
	}

	@Override
	public MatchResultsReport readMatchReport() {
		List<MatchResult> results = readMatchResults();
		List<Scorer> topScorers = readTopScorers();
		List<GroupStanding> groupStandings = readGroupStandings();
		return matchResultFactory.createMatchresultsReport(results, topScorers, groupStandings);		
	}

	private List<MatchResult> readMatchResults() {
		//System.out.println("Reading match results...");
		List<MatchResult> results = new ArrayList<MatchResult>();
		Connection conn = connectionProvider.getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement(SELECT_MATCH_RESULTS_STMT);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				results.add(
					matchResultFactory.createMatchResult(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getInt(6))
				);
			}
			rs.close();
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
		return results;	
	}

	private List<Scorer> readTopScorers() {
		//System.out.println("Reading top scorers...");
		List<Scorer> topScorers = new ArrayList<Scorer>();
		Connection conn = connectionProvider.getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement(SELECT_TOP_SCORERS_STMT);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				topScorers.add(
					matchResultFactory.createScorer(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4))
				);
			}
			rs.close();
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
		return topScorers;	
	}

	private List<GroupStanding> readGroupStandings() {
		//System.out.println("Reading groups standings...");
		List<GroupStanding> groupStandings = new ArrayList<GroupStanding>();
		Connection conn = connectionProvider.getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement(SELECT_GROUP_STANDINGS_STMT);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				groupStandings.add(
					matchResultFactory.createGroupStanding(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5),
							rs.getInt(6), rs.getInt(7), rs.getInt(8))
				);
			}
			rs.close();
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
		return groupStandings;	
	}

}
