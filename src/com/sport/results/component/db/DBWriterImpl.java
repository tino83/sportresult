package com.sport.results.component.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sport.results.container.db.ConnectionProvider;
import com.sport.results.model.db.DBWriter;
import com.sport.results.model.summary.Card;
import com.sport.results.model.summary.Details;
import com.sport.results.model.summary.Goal;
import com.sport.results.model.summary.MatchSummary;
import com.sport.results.model.summary.Player;
import com.sport.results.model.summary.PlayerStats;
import com.sport.results.model.summary.Substitution;
import com.sport.results.model.summary.Team;

public class DBWriterImpl implements DBWriter {
	
	private final ConnectionProvider connectionProvider;

	private static String GET_MAX_TEAM_ID_STMT = "SELECT MAX(TEAM_ID) FROM MATCH_TEAMS";
	private static String INSERT_TEAM_STMT = "INSERT INTO MATCH_TEAMS(TEAM_ID, TEAM_CD, TEAM_NAME) SELECT ?,?,? FROM MATCH_DUAL " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_TEAMS X WHERE X.TEAM_CD = ?)";
	
	private static String GET_MAX_PLAYER_ID_STMT = "SELECT MAX(PLAYER_ID) FROM MATCH_PLAYERS";
	private static String INSERT_PLAYER_STMT = "INSERT INTO MATCH_PLAYERS(PLAYER_ID, TEAM_ID, PLAYER_NAME) SELECT ?,T.TEAM_ID,? FROM MATCH_TEAMS T " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_PLAYERS X WHERE X.TEAM_ID = T.TEAM_ID AND X.PLAYER_NAME = ?) AND T.TEAM_CD = ?";

	private static String GET_MAX_MATCH_ID_STMT = "SELECT MAX(MATCH_ID) FROM MATCH_DETAILS";
	private static String INSERT_MATCH_STMT = "INSERT INTO MATCH_DETAILS(MATCH_ID, MATCH_DATE, MATCH_PLACE, HOME_TEAM_ID, AWAY_TEAM_ID, RANKING_COEFFICIENT) " +
			" SELECT ?,?,?,T1.TEAM_ID,T2.TEAM_ID,1 FROM MATCH_TEAMS T1, MATCH_TEAMS T2 " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_DETAILS X WHERE X.MATCH_DATE = ? AND X.MATCH_PLACE = ?) " +
			" AND T1.TEAM_CD = ? AND T2.TEAM_CD = ?";

	private static String GET_MAX_LINEUP_ID_STMT = "SELECT MAX(LINEUP_ID) FROM MATCH_LINEUPS";
	private static String INSERT_LINEUP_STMT = "INSERT INTO MATCH_LINEUPS(LINEUP_ID, MATCH_ID, TEAM_ID, PLAYER_NUMBER, PLAYER_ID, IS_STARTING, NUM_SHOTS_ON_TARGET, NUM_SHOTS_WIDE, NUM_FOULS, NUM_PASSES_COMPLETED, NUM_PASSES_INTERCEPTED) " +
			" SELECT ?,M.MATCH_ID,T.TEAM_ID,?,P.PLAYER_ID,?,?,?,?,?,? FROM MATCH_DETAILS M, MATCH_TEAMS T, MATCH_PLAYERS P " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_LINEUPS X WHERE X.MATCH_ID = M.MATCH_ID AND X.TEAM_ID = T.TEAM_ID AND X.PLAYER_NUMBER = ?) " +
			" AND M.MATCH_DATE = ? AND M.MATCH_PLACE = ? AND T.TEAM_CD = ? AND P.TEAM_ID = T.TEAM_ID AND P.PLAYER_NAME = ?";	

	private static String GET_MAX_CARD_ID_STMT = "SELECT MAX(CARD_ID) FROM MATCH_CARDS";
	private static String INSERT_CARD_STMT = "INSERT INTO MATCH_CARDS(CARD_ID,MATCH_ID,TEAM_ID,PLAYER_ID,CARD_MINUTE,CARD_TYPE) " +
			" SELECT ?,M.MATCH_ID,T.TEAM_ID,P.PLAYER_ID,?,? FROM MATCH_DETAILS M, MATCH_TEAMS T, MATCH_PLAYERS P " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_CARDS X WHERE X.MATCH_ID = M.MATCH_ID AND X.TEAM_ID = T.TEAM_ID AND X.PLAYER_ID = P.PLAYER_ID " +
				" AND X.CARD_MINUTE = ? AND CARD_TYPE = ?) " +
			" AND M.MATCH_DATE = ? AND M.MATCH_PLACE = ? AND T.TEAM_CD = ? AND P.TEAM_ID = T.TEAM_ID AND P.PLAYER_NAME = ?";

	private static String GET_MAX_GOAL_ID_STMT = "SELECT MAX(GOAL_ID) FROM MATCH_GOALS";
	private static String INSERT_GOAL_STMT = "INSERT INTO MATCH_GOALS(GOAL_ID,MATCH_ID,TEAM_ID,PLAYER_ID,GOAL_MINUTE) " +
			" SELECT ?,M.MATCH_ID,T.TEAM_ID,P.PLAYER_ID,? FROM MATCH_DETAILS M, MATCH_TEAMS T, MATCH_PLAYERS P " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_GOALS X WHERE X.MATCH_ID = M.MATCH_ID AND X.TEAM_ID = T.TEAM_ID AND X.PLAYER_ID = P.PLAYER_ID " +
				" AND X.GOAL_MINUTE = ?) " +
			" AND M.MATCH_DATE = ? AND M.MATCH_PLACE = ? AND T.TEAM_CD = ? AND P.TEAM_ID = T.TEAM_ID AND P.PLAYER_NAME = ?";

	private static String GET_MAX_SUBSTITUTION_ID_STMT = "SELECT MAX(SUBSTITUTION_ID) FROM MATCH_SUBSTITUTIONS";
	private static String INSERT_SUBSTITUTION_STMT = "INSERT INTO MATCH_SUBSTITUTIONS(SUBSTITUTION_ID,MATCH_ID,TEAM_ID,PLAYER_OFF_ID" + 
				",PLAYER_ON_ID,SUBSTITUTION_MINUTE) " +
			" SELECT ?,M.MATCH_ID,T.TEAM_ID,P1.PLAYER_ID,P2.PLAYER_ID,? FROM MATCH_DETAILS M, MATCH_TEAMS T, MATCH_PLAYERS P1, MATCH_PLAYERS P2 " +
			" WHERE NOT EXISTS (SELECT 1 FROM MATCH_SUBSTITUTIONS X WHERE X.MATCH_ID = M.MATCH_ID AND X.TEAM_ID = T.TEAM_ID AND X.PLAYER_OFF_ID = P1.PLAYER_ID " +
				" AND X.PLAYER_ON_ID = P2.PLAYER_ID AND X.SUBSTITUTION_MINUTE = ?) " +
			" AND M.MATCH_DATE = ? AND M.MATCH_PLACE = ? AND T.TEAM_CD = ? AND P1.TEAM_ID = T.TEAM_ID AND P1.PLAYER_NAME = ? " +
				" AND P2.TEAM_ID = T.TEAM_ID AND P2.PLAYER_NAME = ?";

	public DBWriterImpl(ConnectionProvider connectionProvider) {
		this.connectionProvider = connectionProvider;
		try {
			this.connectionProvider.getConnection().setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void writeMatchSummary(MatchSummary summary) {
		try {
			int teamId = getMaxId(GET_MAX_TEAM_ID_STMT) + 1;
			Team homeTeam = summary.getHomeTeam();
			Team awayTeam = summary.getAwayTeam();
			if (writeTeamRecord(homeTeam, teamId)) {
				teamId++;
			}
			writeTeamRecord(awayTeam, teamId);

			int detailsId = getMaxId(GET_MAX_MATCH_ID_STMT) + 1;
			writeMatchDetailsRecord(summary.getDetails(), detailsId, homeTeam.getCode(), awayTeam.getCode());

			int playerId = getMaxId(GET_MAX_PLAYER_ID_STMT) + 1;
			int lineupId = getMaxId(GET_MAX_LINEUP_ID_STMT) + 1;
			for (Player p : summary.getHomeLineup().getStartingPlayers()) {
				if (writePlayerRecord(p, playerId, homeTeam.getCode())) {
					playerId++;
				}
				PlayerStats ps = summary.getHomeTeamStats().getPlayerStats(p);
				writeLineupRecord(lineupId++, summary.getDetails(), p, homeTeam.getCode(), true,
						ps.getNumberOfShotsOnTarget(), ps.getNumberOfShotsWide(), ps.getNumberOfFouls(), ps.getNumberOfPassesCompleted(), ps.getNumberOfPassesIntercepted());
			}
			for (Player p : summary.getHomeLineup().getSubstitutePlayers()) {
				if (writePlayerRecord(p, playerId, homeTeam.getCode())) {
					playerId++;
				}
				PlayerStats ps = summary.getHomeTeamStats().getPlayerStats(p);
				writeLineupRecord(lineupId++, summary.getDetails(), p, homeTeam.getCode(), false,
						ps.getNumberOfShotsOnTarget(), ps.getNumberOfShotsWide(), ps.getNumberOfFouls(), ps.getNumberOfPassesCompleted(), ps.getNumberOfPassesIntercepted());
			}
			for (Player p : summary.getAwayLineup().getStartingPlayers()) {
				if (writePlayerRecord(p, playerId, awayTeam.getCode())) {
					playerId++;
				}
				PlayerStats ps = summary.getAwayTeamStats().getPlayerStats(p);
				writeLineupRecord(lineupId++, summary.getDetails(), p, awayTeam.getCode(), true,
						ps.getNumberOfShotsOnTarget(), ps.getNumberOfShotsWide(), ps.getNumberOfFouls(), ps.getNumberOfPassesCompleted(), ps.getNumberOfPassesIntercepted());
			}
			for (Player p : summary.getAwayLineup().getSubstitutePlayers()) {
				if (writePlayerRecord(p, playerId, awayTeam.getCode())) {
					playerId++;
				}
				PlayerStats ps = summary.getAwayTeamStats().getPlayerStats(p);
				writeLineupRecord(lineupId++, summary.getDetails(), p, awayTeam.getCode(), false,
						ps.getNumberOfShotsOnTarget(), ps.getNumberOfShotsWide(), ps.getNumberOfFouls(), ps.getNumberOfPassesCompleted(), ps.getNumberOfPassesIntercepted());
			}

			int cardId = getMaxId(GET_MAX_CARD_ID_STMT) + 1;
			for (Card c : summary.getHomeEvents().getCards()) {
				writeCardRecord(c, cardId++, summary.getDetails(), homeTeam.getCode());
			}
			for (Card c : summary.getAwayEvents().getCards()) {
				writeCardRecord(c, cardId++, summary.getDetails(), awayTeam.getCode());
			}

			int goalId = getMaxId(GET_MAX_GOAL_ID_STMT) + 1;
			for (Goal g : summary.getHomeEvents().getGoals()) {
				writeGoalRecord(g, goalId++, summary.getDetails(), homeTeam.getCode());
			}
			for (Goal g : summary.getAwayEvents().getGoals()) {
				writeGoalRecord(g, goalId++, summary.getDetails(), awayTeam.getCode());
			}

			int substitutionId = getMaxId(GET_MAX_SUBSTITUTION_ID_STMT) + 1;
			for (Substitution s : summary.getHomeEvents().getSubstitutions()) {
				writeSubstitutionRecord(s, substitutionId++, summary.getDetails(), homeTeam.getCode());
			}
			for (Substitution s : summary.getAwayEvents().getSubstitutions()) {
				writeSubstitutionRecord(s, substitutionId++, summary.getDetails(), awayTeam.getCode());
			}

		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
	}

	private int getMaxId(String query) throws SQLException {
		int id = -1;
		Connection conn = connectionProvider.getConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		if (rs.next()) {
			id = rs.getInt(1);
		} else {
			id = 1;
		}
		rs.close();
		return id;
	}

	private boolean writeTeamRecord(Team team, int teamId) throws SQLException {
		//System.out.println("Writing team record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_TEAM_STMT);
		stmt.setInt(1, teamId);
		stmt.setString(2, team.getCode());
		stmt.setString(3, team.getCode());
		stmt.setString(4, team.getCode());
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);
	}

	private boolean writePlayerRecord(Player player, int playerId, String teamCode) throws SQLException {
		//System.out.println("Writing player record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_PLAYER_STMT);
		stmt.setInt(1, playerId);
		stmt.setString(2, player.getName());
		stmt.setString(3, player.getName());
		stmt.setString(4, teamCode);
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);
	}

	private boolean writeMatchDetailsRecord(Details details, int detailsId, String homeTeamCode, String awayTeamCode) throws SQLException {
		//System.out.println("Writing match record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_MATCH_STMT);
		stmt.setInt(1, detailsId);
		stmt.setDate(2, java.sql.Date.valueOf(details.getDateTime().toLocalDate()));
		stmt.setString(3, details.getPlace());
		stmt.setDate(4, java.sql.Date.valueOf(details.getDateTime().toLocalDate()));
		stmt.setString(5, details.getPlace());
		stmt.setString(6, homeTeamCode);
		stmt.setString(7, awayTeamCode);
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);
	}

	private boolean writeLineupRecord(int lineupId, Details details, Player player, String teamCode, boolean isStarting,
			int numShotsOnTarget, int numShotsWide, int numFouls, int numPassesCompleted, int numPassesIntercepted) throws SQLException {
		//System.out.println("Writing lineup record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_LINEUP_STMT);
		stmt.setInt(1, lineupId);
		stmt.setInt(2, player.getNumber());
		stmt.setInt(3, isStarting ? 1 : 0);
		stmt.setInt(4, numShotsWide);
		stmt.setInt(5, numShotsOnTarget);
		stmt.setInt(6, numFouls);
		stmt.setInt(7, numPassesCompleted);
		stmt.setInt(8, numPassesIntercepted);
		stmt.setInt(9, player.getNumber());
		stmt.setDate(10, java.sql.Date.valueOf(details.getDateTime().toLocalDate()));
		stmt.setString(11, details.getPlace());
		stmt.setString(12, teamCode);
		stmt.setString(13, player.getName());
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);
	}

	private boolean writeCardRecord(Card card, int cardId, Details details, String teamCode) throws SQLException {
		//System.out.println("Writing card record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_CARD_STMT);
		stmt.setInt(1, cardId);
		stmt.setInt(2, card.getMinute());
		stmt.setString(3, card.getCardType().getCode());
		stmt.setInt(4, card.getMinute());
		stmt.setString(5, card.getCardType().getCode());
		stmt.setDate(6, java.sql.Date.valueOf(details.getDateTime().toLocalDate()));
		stmt.setString(7, details.getPlace());
		stmt.setString(8, teamCode);
		stmt.setString(9, card.getPlayer().getName());
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);
	}

	private boolean writeGoalRecord(Goal goal, int goalId, Details details, String teamCode) throws SQLException {
		//System.out.println("Writing goal record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_GOAL_STMT);
		stmt.setInt(1, goalId);
		stmt.setInt(2, goal.getMinute());
		stmt.setInt(3, goal.getMinute());
		stmt.setDate(4, java.sql.Date.valueOf(details.getDateTime().toLocalDate()));
		stmt.setString(5, details.getPlace());
		stmt.setString(6, teamCode);
		stmt.setString(7, goal.getPlayer().getName());
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);	
	}

	private boolean writeSubstitutionRecord(Substitution substitution, int substitutionId, Details details, String teamCode) throws SQLException {
		//System.out.println("Writing substitution record...");
		Connection conn = connectionProvider.getConnection();
		PreparedStatement stmt = conn.prepareStatement(INSERT_SUBSTITUTION_STMT);
		stmt.setInt(1, substitutionId);
		stmt.setInt(2, substitution.getMinute());
		stmt.setInt(3, substitution.getMinute());
		stmt.setDate(4, java.sql.Date.valueOf(details.getDateTime().toLocalDate()));
		stmt.setString(5, details.getPlace());
		stmt.setString(6, teamCode);
		stmt.setString(7, substitution.getOffPlayer().getName());
		stmt.setString(8, substitution.getOnPlayer().getName());
		int rowCount = stmt.executeUpdate();
		conn.commit();
		return (rowCount > 0);	
	}

}
