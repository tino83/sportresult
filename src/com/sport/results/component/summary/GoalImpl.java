package com.sport.results.component.summary;

import com.sport.results.model.summary.Goal;
import com.sport.results.model.summary.Player;

public class GoalImpl implements Goal {

	private final int minute;
	private final Player player;

	public GoalImpl(int minute, Player player) {
		this.minute = minute;
		this.player = player;
	}

	@Override
	public int getMinute() {
		return this.minute;
	}

	@Override
	public Player getPlayer() {
		return this.player;
	}

}
