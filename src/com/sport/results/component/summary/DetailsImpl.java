package com.sport.results.component.summary;

import java.time.LocalDateTime;

import com.sport.results.model.summary.Details;

public class DetailsImpl implements Details {

	private final LocalDateTime dateTime;
	private final String place;

	public DetailsImpl(LocalDateTime dateTime, String place) {
		this.dateTime = dateTime;
		this.place = place;
	}

	@Override
	public LocalDateTime getDateTime() {
		return this.dateTime;
	}

	@Override
	public String getPlace() {
		return this.place;
	}

}
