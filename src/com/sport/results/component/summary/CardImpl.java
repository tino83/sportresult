package com.sport.results.component.summary;

import com.sport.results.common.CardType;
import com.sport.results.model.summary.Card;
import com.sport.results.model.summary.Player;

public class CardImpl implements Card {

	private final CardType type;
	private final int minute;
	private final Player player;
	
	public CardImpl(CardType type, int minute, Player player) {
		this.type = type;
		this.minute = minute;
		this.player = player;
	}

	@Override
	public CardType getCardType() {
		return this.type;
	}

	@Override
	public int getMinute() {
		return this.minute;
	}

	@Override
	public Player getPlayer() {
		return this.player;
	}

}
