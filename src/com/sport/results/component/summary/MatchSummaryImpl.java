package com.sport.results.component.summary;

import com.sport.results.model.summary.Details;
import com.sport.results.model.summary.Events;
import com.sport.results.model.summary.Lineup;
import com.sport.results.model.summary.MatchSummary;
import com.sport.results.model.summary.Team;
import com.sport.results.model.summary.TeamStats;

public class MatchSummaryImpl implements MatchSummary {

	private final Details details;
	private final Team homeTeam;
	private final Team awayTeam;
	private final Lineup homeLineup;
	private final Lineup awayLineup;
	private final Events homeEvents;
	private final Events awayEvents;
	private final TeamStats homeTeamStats;
	private final TeamStats awayTeamStats;

	public MatchSummaryImpl(Details details, Team homeTeam, Team awayTeam, Lineup homeLineup, Lineup awayLineup,
			Events homeEvents, Events awayEvents, TeamStats homeTeamStats, TeamStats awayTeamStats) {
		this.details = details;
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.homeLineup = homeLineup;
		this.awayLineup = awayLineup;
		this.homeEvents = homeEvents;
		this.awayEvents = awayEvents;
		this.homeTeamStats = homeTeamStats;
		this.awayTeamStats = awayTeamStats;
	}

	@Override
	public Details getDetails() {
		return this.details;
	}

	@Override
	public Team getHomeTeam() {
		return this.homeTeam;
	}

	@Override
	public Team getAwayTeam() {
		return this.awayTeam;
	}

	@Override
	public Lineup getHomeLineup() {
		return this.homeLineup;
	}

	@Override
	public Lineup getAwayLineup() {
		return this.awayLineup;
	}

	@Override
	public Events getHomeEvents() {
		return this.homeEvents;
	}

	@Override
	public Events getAwayEvents() {
		return this.awayEvents;
	}

	@Override
	public TeamStats getHomeTeamStats() {
		return this.homeTeamStats;
	}

	@Override
	public TeamStats getAwayTeamStats() {
		return this.awayTeamStats;
	}

}
