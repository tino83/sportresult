package com.sport.results.component.summary;

import com.sport.results.model.summary.PlayerStats;

public class PlayerStatsImpl implements PlayerStats {

	private final int numberOfShotsOnTarget;
	private final int numberOfShotsWide;
	private final int numberOfFouls;
	private final int numberOfPassesCompleted;
	private final int numberOfPassesIntercepted;

	public PlayerStatsImpl(int numberOfShotsOnTarget, int numberOfShotsWide, int numberOfFouls,
			int numberOfPassesCompleted, int numberOfPassesIntercepted) {
		this.numberOfShotsOnTarget = numberOfShotsOnTarget;
		this.numberOfShotsWide = numberOfShotsWide;
		this.numberOfFouls = numberOfFouls;
		this.numberOfPassesCompleted = numberOfPassesCompleted;
		this.numberOfPassesIntercepted = numberOfPassesIntercepted;
	}

	@Override
	public int getNumberOfShotsOnTarget() {
		return this.numberOfShotsOnTarget;
	}

	@Override
	public int getNumberOfShotsWide() {
		return this.numberOfShotsWide;
	}

	@Override
	public int getNumberOfFouls() {
		return this.numberOfFouls;
	}

	@Override
	public int getNumberOfPassesCompleted() {
		return this.numberOfPassesCompleted;
	}

	@Override
	public int getNumberOfPassesIntercepted() {
		return this.numberOfPassesIntercepted;
	}

}
