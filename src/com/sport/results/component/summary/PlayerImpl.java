package com.sport.results.component.summary;

import com.sport.results.model.summary.Player;

public class PlayerImpl implements Player {

	private final int number;
	private final String name;

	public PlayerImpl(int number, String name) {
		this.number = number;
		this.name = name;
	}

	@Override
	public int getNumber() {
		return this.number;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
