package com.sport.results.component.summary;

import java.util.Map;

import com.sport.results.model.summary.Player;
import com.sport.results.model.summary.PlayerStats;
import com.sport.results.model.summary.TeamStats;

public class TeamStatsImpl implements TeamStats {

	private final Map<Player, PlayerStats> playerStats;

	public TeamStatsImpl(Map<Player, PlayerStats> playerStats) {
		this.playerStats = playerStats;
	}

	@Override
	public PlayerStats getPlayerStats(Player player) {
		return this.playerStats.get(player);
	}

}
