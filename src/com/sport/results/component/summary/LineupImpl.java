package com.sport.results.component.summary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sport.results.model.summary.Lineup;
import com.sport.results.model.summary.Player;

public class LineupImpl implements Lineup {

	private final List<Player> starting;
	private final List<Player> substitute;
	private final Map<Integer, Player> allPlayers = new HashMap<Integer, Player>();
	private final List<Integer> playerNumbersSorted = new ArrayList<Integer>();

	public LineupImpl(List<Player> starting, List<Player> substitute) {
		this.starting = starting;
		this.substitute = substitute;
		for (Player p : starting) {
			this.allPlayers.put(p.getNumber(), p);
			this.playerNumbersSorted.add(p.getNumber());
		}
		for (Player p : substitute) {
			this.allPlayers.put(p.getNumber(), p);
			this.playerNumbersSorted.add(p.getNumber());
		}
		Collections.sort(this.playerNumbersSorted);
	}

	@Override
	public Iterable<Player> getStartingPlayers() {
		return this.starting;
	}

	@Override
	public Iterable<Player> getSubstitutePlayers() {
		return this.substitute;
	}

	@Override
	public Player getPlayerByNumber(int number) {
		return this.allPlayers.get(number);
	}

	@Override
	public Player getPlayerByOrdinal(int ordinal) {
		if (ordinal > -1 && ordinal < this.playerNumbersSorted.size()) {
			return this.getPlayerByNumber(this.playerNumbersSorted.get(ordinal));
		} else {
			return null;
		}
	}

}
