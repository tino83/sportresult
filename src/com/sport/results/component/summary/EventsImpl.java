package com.sport.results.component.summary;

import java.util.List;

import com.sport.results.model.summary.Card;
import com.sport.results.model.summary.Events;
import com.sport.results.model.summary.Goal;
import com.sport.results.model.summary.Substitution;

public class EventsImpl implements Events {

	private final List<Card> cards;
	private final List<Goal> goals;
	private final List<Substitution> substitutions;

	public EventsImpl(List<Card> cards, List<Goal> goals, List<Substitution> substitutions) {
		this.cards = cards;
		this.goals = goals;
		this.substitutions = substitutions;
	}

	@Override
	public Iterable<Card> getCards() {
		return this.cards;
	}

	@Override
	public Iterable<Goal> getGoals() {
		return this.goals;
	}

	@Override
	public Iterable<Substitution> getSubstitutions() {
		return this.substitutions;
	}

}
