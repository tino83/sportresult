package com.sport.results.component.summary;

import com.sport.results.model.summary.Player;
import com.sport.results.model.summary.Substitution;

public class SubstitutionImpl implements Substitution {

	private final int minute;
	private final Player off;
	private final Player on;

	public SubstitutionImpl(int minute, Player off, Player on) {
		this.minute = minute;
		this.off = off;
		this.on = on;
	}

	@Override
	public int getMinute() {
		return this.minute;
	}

	@Override
	public Player getOffPlayer() {
		return this.off;
	}

	@Override
	public Player getOnPlayer() {
		return this.on;
	}

}
