package com.sport.results.container.factory.report;

import java.util.List;

import com.sport.results.component.report.GroupStandingImpl;
import com.sport.results.component.report.MatchResultImpl;
import com.sport.results.component.report.MatchResultsReportImpl;
import com.sport.results.component.report.ScorerImpl;
import com.sport.results.component.report.TeamImpl;
import com.sport.results.component.report.PlayerImpl;
import com.sport.results.model.report.GroupStanding;
import com.sport.results.model.report.MatchResult;
import com.sport.results.model.report.MatchResultsReport;
import com.sport.results.model.report.Scorer;
import com.sport.results.model.report.Team;
import com.sport.results.model.report.Player;

public class MatchResultFactory {

	public Team createTeam(String code, String name) {
		return new TeamImpl(code, name);
	}

	public Player createPlayer(String teamCode, String teamName, String playerName) {
		Team team = this.createTeam(teamCode, teamName);
		return new PlayerImpl(team, playerName);
	}

	public MatchResult createMatchResult(String homeTeamCode, String homeTeamName, int homeTeamGoalCount,
			String awayTeamCode, String awayTeamName, int awayTeamGoalCount) {
		Team homeTeam = this.createTeam(homeTeamCode, homeTeamName);
		Team awayTeam = this.createTeam(awayTeamCode, awayTeamName);
		return new MatchResultImpl(homeTeam, awayTeam, homeTeamGoalCount, awayTeamGoalCount);
	}

	public Scorer createScorer(String teamCode, String teamName, String playerName, int goalCount) {
		Player player = this.createPlayer(teamCode, playerName, teamName);
		return new ScorerImpl(player, goalCount);
	}

	public GroupStanding createGroupStanding(String teamCode, String teamName, int wins, int ties, int loses,
			int points, int goalsFor, int goalsAgainst) {
		Team team = this.createTeam(teamCode, teamName);
		return new GroupStandingImpl(team, wins, ties, loses, points, goalsFor, goalsAgainst);
	}

	public MatchResultsReport createMatchresultsReport(List<MatchResult> results, List<Scorer> scorers, List<GroupStanding> groupStandings) {
		return new MatchResultsReportImpl(results, scorers, groupStandings);
	}

}
