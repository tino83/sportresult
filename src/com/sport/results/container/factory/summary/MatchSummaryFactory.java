package com.sport.results.container.factory.summary;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sport.results.common.CardType;
import com.sport.results.component.summary.CardImpl;
import com.sport.results.component.summary.DetailsImpl;
import com.sport.results.component.summary.EventsImpl;
import com.sport.results.component.summary.GoalImpl;
import com.sport.results.component.summary.LineupImpl;
import com.sport.results.component.summary.MatchSummaryImpl;
import com.sport.results.component.summary.PlayerImpl;
import com.sport.results.component.summary.PlayerStatsImpl;
import com.sport.results.component.summary.SubstitutionImpl;
import com.sport.results.component.summary.TeamImpl;
import com.sport.results.component.summary.TeamStatsImpl;
import com.sport.results.model.summary.Card;
import com.sport.results.model.summary.Details;
import com.sport.results.model.summary.Events;
import com.sport.results.model.summary.Goal;
import com.sport.results.model.summary.Lineup;
import com.sport.results.model.summary.MatchSummary;
import com.sport.results.model.summary.Player;
import com.sport.results.model.summary.PlayerStats;
import com.sport.results.model.summary.Substitution;
import com.sport.results.model.summary.Team;
import com.sport.results.model.summary.TeamStats;

public class MatchSummaryFactory {

	public Details createDetails(String dateTime, String place) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
		LocalDateTime dt = LocalDateTime.parse(dateTime, formatter); 
		return new DetailsImpl(dt, place);
	}

	public Team createTeam(String code, String name) {
		return new TeamImpl(code, name);
	}

	public Player createPlayer(String number, String name) {
		int n;
		try {
			n = Integer.parseInt(number);
		} catch(Exception e) {
			throw new RuntimeException("Invalid player number!");
		}
		return new PlayerImpl(n, name);
	}

	public Card createCard(String type, String minute, String number, Lineup lineup) {
		int m;
		try {
			m = Integer.parseInt(minute);
		} catch(Exception e) {
			throw new RuntimeException("Invalid minute!");
		}
		int n;
		try {
			n = Integer.parseInt(number);
		} catch(Exception e) {
			throw new RuntimeException("Invalid player number!");
		}
		if (type.equalsIgnoreCase("Y")) {
			return new CardImpl(CardType.YELLOW, m, lineup.getPlayerByNumber(n));
		} else if (type.equalsIgnoreCase("R")) {
			return new CardImpl(CardType.RED, m, lineup.getPlayerByNumber(n));
		} else {
			throw new RuntimeException("Invalid card type!");
		}
	}

	public Goal createGoal(String minute, String number, Lineup lineup) {
		int m;
		try {
			m = Integer.parseInt(minute);
		} catch(Exception e) {
			throw new RuntimeException("Invalid minute!");
		}
		int n;
		try {
			n = Integer.parseInt(number);
		} catch(Exception e) {
			throw new RuntimeException("Invalid player number!");
		}
		return new GoalImpl(m, lineup.getPlayerByNumber(n));
	}

	public Substitution createSubstitution(String minute, String numberOff, String numberOn, Lineup lineup) {
		int m;
		try {
			m = Integer.parseInt(minute);
		} catch(Exception e) {
			throw new RuntimeException("Invalid minute!");
		}
		int noff;
		try {
			noff = Integer.parseInt(numberOff);
		} catch(Exception e) {
			throw new RuntimeException("Invalid player number!");
		}
		int non;
		try {
			non = Integer.parseInt(numberOn);
		} catch(Exception e) {
			throw new RuntimeException("Invalid player number!");
		}
		return new SubstitutionImpl(m, lineup.getPlayerByNumber(non), lineup.getPlayerByNumber(noff));
	}

	public Lineup createLineup(String[][] starting, String[][] substitute) {
		List<Player> startingList = new ArrayList<Player>();
		List<Player> substituteList = new ArrayList<Player>();
		for (String[] p : starting) {
			startingList.add(this.createPlayer(p[0], p[1]));
		}
		for (String[] p : substitute) {
			substituteList.add(this.createPlayer(p[0], p[1]));
		}
		return new LineupImpl(startingList, substituteList);
	}

	public Events createEvents(String[][] cards, String[][] goals, String[][] substitutions, Lineup lineup) {
		List<Card> cardsList = new ArrayList<Card>();
		List<Substitution> substitutionsList = new ArrayList<Substitution>();
		for (String[] p : cards) {
			cardsList.add(this.createCard(p[0], p[1], p[2], lineup));
		}
		List<Goal> goalsList = null;
		for (String[] p : goals) {
			if (goalsList == null) {
				goalsList = new ArrayList<Goal>();
			}
			goalsList.add(this.createGoal(p[0], p[1], lineup));
		}
		for (String[] p : substitutions) {
			substitutionsList.add(this.createSubstitution(p[0], p[1], p[2], lineup));
		}
		return new EventsImpl(cardsList, goalsList, substitutionsList);
	}

	public TeamStats createTeamStats(String[][] shots, String[] fouls, String[][] passes, Lineup lineup) {
		Map<Player, PlayerStats> stats = new HashMap<Player, PlayerStats>();
		for (int i = 0; i < shots.length; i++) {
			int st;
			try {
				st = Integer.parseInt(shots[i][0]);
			} catch(Exception e) {
				throw new RuntimeException("Invalid number of shots on target!");
			}
			int as;
			try {
				as = Integer.parseInt(shots[i][1]);
			} catch(Exception e) {
				throw new RuntimeException("Invalid number of all shots!");
			}
			int sw = as - st;
			int f;
			try {
				f = Integer.parseInt(fouls[i]);
			} catch(Exception e) {
				throw new RuntimeException("Invalid number of fouls!");
			}
			int pc;
			try {
				pc = Integer.parseInt(passes[i][0]);
			} catch(Exception e) {
				throw new RuntimeException("Invalid number of passes completed!");
			}
			int ap;
			try {
				ap = Integer.parseInt(passes[i][1]);
			} catch(Exception e) {
				throw new RuntimeException("Invalid number of all passes!");
			}
			int pi = ap - pc;
			stats.put(lineup.getPlayerByOrdinal(i), new PlayerStatsImpl(st, sw, f, pc, pi));
		}
		return new TeamStatsImpl(stats);
	}

	public MatchSummary createMatchSummary(String[] detail, String[][] teams, String[][][][] lineups, String[][][] cards,
			String[][][] goals, String[][][] substitutions, String[][][] shots, String[][] fouls, String[][][] passes) {
		Details details = this.createDetails(detail[0], detail[1]);
		Team homeTeam = this.createTeam(teams[0][0], teams[0][1]);
		Team awayTeam = this.createTeam(teams[1][0], teams[1][1]);
		Lineup homeLineup = this.createLineup(lineups[0][0], lineups[0][1]);
		Lineup awayLineup = this.createLineup(lineups[1][0], lineups[1][1]);
		Events homeEvents = this.createEvents(cards[0], goals[0], substitutions[0], homeLineup);
		Events awayEvents = this.createEvents(cards[1], goals[1], substitutions[1], awayLineup);
		TeamStats homeTeamStats = this.createTeamStats(shots[0], fouls[0], passes[0], homeLineup);
		TeamStats awayTeamStats = this.createTeamStats(shots[1], fouls[1], passes[1], awayLineup);
		return new MatchSummaryImpl(details, homeTeam, awayTeam, homeLineup, awayLineup,
				homeEvents, awayEvents, homeTeamStats, awayTeamStats);
	}

	public MatchSummary createMatchSummary(Details details, Team homeTeam, Team awayTeam, Lineup homeLineup, Lineup awayLineup,
			Events homeEvents, Events awayEvents, TeamStats homeTeamStats, TeamStats awayTeamStats) {
		return new MatchSummaryImpl(details, homeTeam, awayTeam, homeLineup, awayLineup,
				homeEvents, awayEvents, homeTeamStats, awayTeamStats);
	}

}
