package com.sport.results.container;

import com.sport.results.common.config.Configuration;
import com.sport.results.common.db.DBSettings;
import com.sport.results.component.db.DBReaderImpl;
import com.sport.results.component.db.DBWriterImpl;
import com.sport.results.component.io.parser.ParsersManagerImpl;
import com.sport.results.component.io.reader.MatchSummaryReaderImpl;
import com.sport.results.component.report.MatchReportFormatterImpl;
import com.sport.results.container.db.ConnectionProvider;
import com.sport.results.container.factory.report.MatchResultFactory;
import com.sport.results.container.factory.summary.MatchSummaryFactory;
import com.sport.results.model.db.DBReader;
import com.sport.results.model.db.DBWriter;
import com.sport.results.model.io.parser.ParsersManager;
import com.sport.results.model.io.reader.MatchSummaryReader;
import com.sport.results.model.report.MatchReportFormatter;

public class Container {

	private static Container container;
	private final MatchSummaryFactory matchSummaryFactory;
	private final MatchResultFactory matchResultFactory;
	private final ConnectionProvider connectionProvider;
	private final DBWriter dBWriter;
	private final DBReader dbRreader;
	private final MatchReportFormatter formatter;
	private final ParsersManager parsersManager;
	private final MatchSummaryReader matchSummaryReader;

	public static void createInstance(Configuration configuration) {
		if (container == null) {
			container = new Container(configuration);
		} else {
			throw new RuntimeException("Container already initialized!");
		}
	}

	public static Container getInstance() {
		if (container == null) {
			throw new RuntimeException("Container not initialized!");
		} else {
			return container;
		}
	}

	private Container(Configuration configuration) {
		this.matchSummaryFactory = new MatchSummaryFactory();
		this.matchResultFactory = new MatchResultFactory();
		this.connectionProvider = new ConnectionProvider(new DBSettings(configuration));
		this.dBWriter = new DBWriterImpl(connectionProvider);
		this.dbRreader = new DBReaderImpl(connectionProvider, matchResultFactory);
		this.formatter = new MatchReportFormatterImpl();
		this.parsersManager = new ParsersManagerImpl(matchSummaryFactory);
		this.matchSummaryReader = new MatchSummaryReaderImpl(parsersManager);
	}

	public MatchSummaryFactory getMatchSummaryFactory() {
		return this.matchSummaryFactory;
	}

	public MatchResultFactory getMatchResultFactory() {
		return this.matchResultFactory;
	}

	public ConnectionProvider getConnectionProvider() {
		return this.connectionProvider;
	}

	public DBWriter getDBWriter() {
		return this.dBWriter;
	}

	public DBReader getDBReader() {
		return this.dbRreader;
	}

	public MatchReportFormatter getMatchReportFormatter() {
		return this.formatter;
	}

	public ParsersManager getParsersManager() {
		return this.parsersManager;
	}

	public MatchSummaryReader getMatchSummaryReader() {
		return this.matchSummaryReader;
	}

}
