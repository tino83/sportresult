package com.sport.results.container.db;


import java.sql.Connection;
import java.sql.DriverManager;

import com.sport.results.common.db.DBSettings;
import com.sport.results.common.db.DBType;

public class ConnectionProvider {

	private final DBSettings dbSettings;
	private Connection connection;

	public ConnectionProvider(DBSettings dbSettings) {
		this.dbSettings = dbSettings;
		this.connection = createConnection();
	}

	public Connection getConnection() {
		try {
			if (this.connection == null) {
				this.connection = createConnection();
				return this.connection;
			} else {
				return this.connection;
			}
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
		return this.connection;
	}

	private Connection createConnection() {
		try {
			if (this.dbSettings.getDBType() == DBType.ORACLE) {
				Class.forName("oracle.jdbc.driver.OracleDriver");
			} else if (this.dbSettings.getDBType() == DBType.MYSQL) {
				//Class.forName("com.mysql.jdbc.Driver");
				Class.forName("com.mysql.cj.jdbc.Driver");
			} else {
				throw new RuntimeException("Unsupported DB!");
			}
			Connection connection = DriverManager.getConnection(this.dbSettings.getUrl(), this.dbSettings.getUser(), this.dbSettings.getPassword());
			return connection;
		} catch(Exception e) {
			e.printStackTrace(System.out);
			return null;
		}
	}

	public void closeConnection() {
		try {
			this.connection.close();
		} catch(Exception e) {
			e.printStackTrace(System.out);
			
		}
		this.connection = null;
	}

}
