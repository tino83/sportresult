package com.sport.results.cli;

public class Parameter extends Argument {

	private final String value;

	public Parameter(ArgumentCode argumentCode, String value) {
		super(argumentCode);
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Parameter) {
			return ((Parameter)o).argumentCode == this.argumentCode;
		} else {
			return false;
		}
	}

}
