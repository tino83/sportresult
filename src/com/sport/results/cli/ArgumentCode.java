package com.sport.results.cli;

public enum ArgumentCode {

	HELP("-h", ArgumentType.FLAG),
	INPUT_FOLDER("-i", ArgumentType.PARAMETER),
	CONFIG_FILE("-c", ArgumentType.PARAMETER);

	private final String code;
	private final ArgumentType type;

	private ArgumentCode(String code, ArgumentType type) {
		this.code = code;
		this.type = type;
	}

	public static ArgumentCode parseCode(String code) {
		for (ArgumentCode f : ArgumentCode.values()) {
			if (code.equalsIgnoreCase(f.code)) {
				return f;
			}
		}
		throw new RuntimeException("Unsupported flag!");
	}

	public String getCode() {
		return this.code;
	}

	public ArgumentType getType() {
		return this.type;
	}

}
