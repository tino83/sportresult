package com.sport.results.cli;

public class Flag extends Argument {

	public Flag(ArgumentCode argumentCode) {
		super(argumentCode);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Flag) {
			return ((Flag)o).argumentCode == this.argumentCode;
		} else {
			return false;
		}
	}

}
