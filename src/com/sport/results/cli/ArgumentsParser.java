package com.sport.results.cli;

import java.util.HashSet;
import java.util.Set;

public class ArgumentsParser {

	public Arguments parseArguments(String[] args) {
		Set<Argument> argSet = new HashSet<Argument>();
		for (int i = 0; i < args.length; i++) {
			ArgumentCode c = ArgumentCode.parseCode(args[i]);
			if (c.getType() == ArgumentType.FLAG) {
				argSet.add(new Flag(c));
			} else if (c.getType() == ArgumentType.PARAMETER) {
				argSet.add(new Parameter(c, args[++i]));
			}
		}
		return new Arguments(argSet);
	}

}
