package com.sport.results.cli;

public abstract class Argument {

	protected final ArgumentCode argumentCode;

	public Argument(ArgumentCode argumentCode) {
		this.argumentCode = argumentCode;
	}

	public ArgumentCode getArgumentCode() {
		return this.argumentCode;
	}

	@Override
	public int hashCode() {
		return this.argumentCode.hashCode();
	}

}
