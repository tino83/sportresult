package com.sport.results.cli;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Arguments {

	private Map<ArgumentCode, Argument> args = new HashMap<ArgumentCode, Argument>();

	public Arguments(Set<Argument> args) {
		for (Argument a : args) {
			this.args.put(a.argumentCode, a);
		}
	}

	public Iterable<Argument> getSet() {
		return this.args.values();
	}

	public boolean containsArgument(ArgumentCode code) {
		return this.args.containsKey(code);
	}

	public Argument getArgument(ArgumentCode code) {
		return this.args.get(code);
	}

}
